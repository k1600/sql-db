/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.kanin.databaseproject.dao;

import java.util.List;
import com.kanin.databaseproject.model.User;
/**
 *
 * @author Jessada
 */

public interface Dao<T> {

    T get(int id);

    List<T> getAll();

    T save(T obj);

    T update(T obj);

    int delete(T obj);
    
    List<T> getAll(String where, String order);
}
